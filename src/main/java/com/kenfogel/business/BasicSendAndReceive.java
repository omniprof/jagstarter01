package com.kenfogel.business;

import java.util.ArrayList;
import java.util.List;

import com.kenfogel.mailbean.MailBean;
import com.kenfogel.properties.MailConfigBean;
import javax.mail.Flags;
import jodd.mail.Email;
import jodd.mail.EmailFilter;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.EmailAddress;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Here is a starter class for phase 1. It sends and receives basic email that
 * consists of a to, from, subject and text.
 *
 * There have been updates to deal with changes in Jodd. We will use 5.1.6
 *
 * @author Ken Fogel
 *
 */
public class BasicSendAndReceive {

    // Real programmers use logging
    private final static Logger LOG = LoggerFactory.getLogger(BasicSendAndReceive.class);

    /**
     * Basic send routine for this sample.It does not handle CC, BCC,
     * attachments or other information.
     *
     * @param mailBean
     * @param sendConfigBean
     * @return
     */
    public final String sendEmail(final MailBean mailBean, final MailConfigBean sendConfigBean) {

        // Create am SMTP server object
        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(sendConfigBean.getHost())
                .auth(sendConfigBean.getUserEmailAddress(), sendConfigBean.getPassword())
                .buildSmtpMailServer();

        // Do not use the fluent type because ArrayLists of To, CC and BCC need 
        // to be processed from an ArrayList in the bean
        Email email = Email.create()
                .from(sendConfigBean.getUserEmailAddress());

        // Add all the address in the ArrayList
        mailBean.getToField().forEach(emailAddress -> {
            if (checkEmail(emailAddress)) {
                email.to(emailAddress);
            }
        });

        email.subject(mailBean.getSubjectField());
        email.textMessage(mailBean.getTextMessageField());

        String messageId = "Fail";
        // A session is the object responsible for communicating with the server
        // Similar to how we work with a file, we open the session, send the 
        // message and close the session. try-with-resources ensures we never 
        // forget to close the session.
        try ( SendMailSession session = smtpServer.createSession()) {
            session.open();
            messageId = session.sendMail(email);
        } catch (Exception ex) {
            LOG.debug("Failure in session", ex);
        }
        return messageId;

    }

    /**
     * Basic receive that only takes the values that match the basic mail bean.
     * Returns an array list because there could be more than one message. This
     * could be a problem during testing so use addresses that do not receive
     * any other messages and you can assume that subscript 0 in the array is
     * the message you just sent.
     *
     * @param receiveConfigBean
     * @return
     */
    public final ArrayList<MailBean> receiveEmail(final MailConfigBean receiveConfigBean) {

        ArrayList<MailBean> mailBeans = null;

        // Create an IMAP server that does not display debug info
        ImapServer imapServer = MailServer.create()
                .host(receiveConfigBean.getHost())
                .ssl(true)
                .auth(receiveConfigBean.getUserEmailAddress(), receiveConfigBean.getPassword())
                .buildImapMailServer();

        // A session is the object responsible for communicating with the server
        // Similar to how we work with a file, we open the session, send the 
        // message and close the session. try-with-resources ensures we never 
        // forget to close the session.
        try ( ReceiveMailSession session = imapServer.createSession()) {
            session.open();

            // We only want messages that have not been read yet.
            // Messages that are delivered are then marked as read on the server
            ReceivedEmail[] emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));

            // If there is any email then loop through them adding their contents to
            // a new MailBean that is then added to the array list.
            if (emails != null) {

                // Instantiate the array list of messages
                mailBeans = new ArrayList<>();

                for (ReceivedEmail email : emails) {

                    MailBean mailBean = new MailBean();

                    mailBean.setFromField(email.from().getEmail());
                    mailBean.setSubjectField(email.subject());
                    for (EmailAddress mailAddress : email.to()) {
                        mailBean.getToField().add(mailAddress.getEmail());
                    }
                    // Messages may be multi-part so they are stored in an array
                    // In this demo we only want the first part
                    List<EmailMessage> messages = email.messages();
                    mailBean.setTextMessageField(messages.get(0).getContent());

                    // Add the mailBean to the array list
                    mailBeans.add(mailBean);
                }
            }

        } catch (Exception ex) {
            LOG.debug("Failure in receive session", ex);

        }
        return mailBeans;
    }

    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
}
